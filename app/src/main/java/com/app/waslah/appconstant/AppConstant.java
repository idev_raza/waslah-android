package com.app.waslah.appconstant;

public interface AppConstant {

    int TYPE_TOP_BANNER = 0;
    int TYPE_CLASS = 1;

    public String data="{\n" +
            "\t\"success\": true,\n" +
            "\t\"message\": \"ok\",\n" +
            "\t\"data\": {\n" +
            "\t\t\"banners\": [{\n" +
            "\t\t\t\t\"path\": \"resources/ecom/banner/banner1.png\",\n" +
            "\t\t\t\t\"id\": 61\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"path\": \"resources/ecom/banner/banner2.png\",\n" +
            "\t\t\t\t\"id\": 71\n" +
            "\t\t\t}\n" +
            "\t\t],\n" +
            "\t\t\"class\": [{\n" +
            "\t\t\t\t\"name\": \"Class 0\",\n" +
            "\t\t\t\t\"id\": 3687,\n" +
            "\t\t\t\t\"website_url\": \"https://www.youtube.com/watch?v=ZgG8kODVsxY&index=102&list=PLxhnpe8pN3TkenzFLTlz2hUd6_BZu-5Zv\",\n" +
            "\t\t\t\t\"duration\": \"Label 0\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"name\": \"Class 1\",\n" +
            "\t\t\t\t\"id\": 4171,\n" +
            "\t\t\t\t\"website_url\": \"https://www.youtube.com/watch?v=nz9c9ar4h64\",\n" +
            "\t\t\t\t\"duration\": \"Label 1\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"name\": \"Class 2\",\n" +
            "\t\t\t\t\"id\": 4171,\n" +
            "\t\t\t\t\"website_url\": \"https://www.youtube.com/watch?v=nz9c9ar4h64\",\n" +
            "\t\t\t\t\"duration\": \"Label 2\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"name\": \"Class 3\",\n" +
            "\t\t\t\t\"id\": 4171,\n" +
            "\t\t\t\t\"website_url\": \"https://www.youtube.com/watch?v=nz9c9ar4h64\",\n" +
            "\t\t\t\t\"duration\": \"Label 3\"\n" +
            "\t\t\t},\n" +
            "\t\t\t{\n" +
            "\t\t\t\t\"name\": \"Class 4\",\n" +
            "\t\t\t\t\"id\": 4171,\n" +
            "\t\t\t\t\"website_url\": \"https://www.youtube.com/watch?v=nz9c9ar4h64\",\n" +
            "\t\t\t\t\"duration\": \"Label 4\"\n" +
            "\t\t\t}\n" +
            "\t\t]\n" +
            "\t}\n" +
            "}";
}
