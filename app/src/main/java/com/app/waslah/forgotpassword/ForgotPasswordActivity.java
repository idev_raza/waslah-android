package com.app.waslah.forgotpassword;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.waslah.R;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView txtHeader;
    private ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initHeader();
    }

    private void initHeader() {
        txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(getResources().getString(R.string.btn_reset_password));
        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                finish();
                break;
        }
    }
}
