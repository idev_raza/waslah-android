package com.app.waslah.changepassword;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.app.waslah.R;

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtHeader;
    private ImageView ivBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initHeader();
    }

    private void initHeader() {
        txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(getResources().getString(R.string.btn_reset_password));
        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                finish();
                break;
        }
    }
}
