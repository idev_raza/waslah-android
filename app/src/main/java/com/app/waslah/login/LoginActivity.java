package com.app.waslah.login;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.app.waslah.R;
import com.app.waslah.forgotpassword.ForgotPasswordActivity;
import com.app.waslah.signup.SignUpActivity;
import com.app.waslah.bottomtab.BottomTabActivity;
import com.app.waslah.utils.JUtils;
import com.google.android.material.snackbar.Snackbar;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvForgotPassword, tvSignUp;
    private Button btnLogin;
    private LinearLayout llMain;
    private EditText etEMail, etPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void showSnackbar(String msg) {
        Snackbar snackbar = Snackbar.make(llMain, msg, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackground(getResources().getDrawable(R.drawable.text_shaded));
        TextView textView = snackBarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.RED);
        snackbar.show();
    }

    private void initView() {
        //
        llMain = findViewById(R.id.llMain);
        //
        etEMail = findViewById(R.id.etEMail);
        etPassword = findViewById(R.id.etPassword);

        //
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        tvSignUp = findViewById(R.id.tvSignUp);
        //
        tvSignUp.setOnClickListener(this);
        tvForgotPassword.setOnClickListener(this);
        //
        btnLogin = findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        //
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvSignUp:
                sendIntentToSignUp();
                break;
            case R.id.tvForgotPassword:
                sendIntentToForgotPassword();
                break;
            case R.id.btnLogin:
                validateLogin();

                break;
        }
    }

    private void sendIntentToSignUp() {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    private void sendIntentToForgotPassword() {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

    private void validateLogin() {
        String email = etEMail.getText().toString();
        String password = etPassword.getText().toString();
        if (TextUtils.isEmpty(email)) {
            showSnackbar("Please enter email");
            return;
        }
        if (!JUtils.isEmailValid(email)) {
            showSnackbar("Please enter valid email");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            showSnackbar("Please enter password");
            return;
        }
        if (password.length() < 7) {
            showSnackbar("Password must be 7 digit long");
            return;
        }
        sendIntentToBottomTab();
    }

    private void sendIntentToBottomTab() {
        startActivity(new Intent(this, BottomTabActivity.class));
        finish();
    }
}
