package com.app.waslah.userinfoscreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.app.waslah.R;
import com.app.waslah.bottomtab.BottomTabActivity;

public class UserInfoActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        LinearLayout llMain = findViewById(R.id.llMain);
        llMain.setOnClickListener(this);
    }

    private void sendIntentToBottomTab() {
        startActivity(new Intent(UserInfoActivity.this, BottomTabActivity.class));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llMain:
                sendIntentToBottomTab();
                break;
        }
    }
}
