package com.app.waslah.dummymodel;

public class LabelModel {
    String label;
    int imageDrawable;

    public LabelModel(String label, int imageDrawable) {
        this.label = label;
        this.imageDrawable = imageDrawable;
    }

    public String getLabel() {
        return label;
    }

    public int getImageDrawable() {
        return imageDrawable;
    }
}
