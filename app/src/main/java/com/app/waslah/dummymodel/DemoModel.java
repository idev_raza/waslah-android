
package com.app.waslah.dummymodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DemoModel {

    @SerializedName("success")
    @Expose
    public Boolean success;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("data")
    @Expose
    public Data data;

    public Boolean getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data {

        @SerializedName("banners")
        @Expose
        public List<Banner> banners = null;
        @SerializedName("class")
        @Expose
        public List<Clas> _class = null;

        //

        public List<Banner> getBanners() {
            return banners;
        }

        public List<Clas> get_class() {
            return _class;
        }

        //


        public class Banner {

            @SerializedName("path")
            @Expose
            public String path;
            @SerializedName("id")
            @Expose
            public Integer id;

            public String getPath() {
                return path;
            }

            public Integer getId() {
                return id;
            }
        }

        public class Clas {

            @SerializedName("name")
            @Expose
            public String name;
            @SerializedName("id")
            @Expose
            public Integer id;
            @SerializedName("website_url")
            @Expose
            public String websiteUrl;
            @SerializedName("duration")
            @Expose
            public String duration;

            public String getName() {
                return name;
            }

            public Integer getId() {
                return id;
            }

            public String getWebsiteUrl() {
                return websiteUrl;
            }

            public String getDuration() {
                return duration;
            }
        }
    }

}
