package com.app.waslah.dummymodel.displaymodel;

import com.app.waslah.dummymodel.DemoModel;

import java.util.ArrayList;

public class HomeDisplayModel {
    int type;
    private ArrayList<DemoModel.Data.Banner> dataBanner;
    private  ArrayList<DemoModel.Data.Clas> dataClass;


    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    //

    public ArrayList<DemoModel.Data.Banner> getDataBanner() {
        return dataBanner;
    }

    public void setDataBanner(ArrayList<DemoModel.Data.Banner> dataBanner) {
        this.dataBanner = dataBanner;
    }

    public ArrayList<DemoModel.Data.Clas> getDataClass() {
        return dataClass;
    }

    public void setDataClass(ArrayList<DemoModel.Data.Clas> dataClass) {
        this.dataClass = dataClass;
    }

    //
}
