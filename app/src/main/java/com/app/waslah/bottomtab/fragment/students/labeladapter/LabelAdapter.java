package com.app.waslah.bottomtab.fragment.students.labeladapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.waslah.R;
import com.app.waslah.dummymodel.LabelModel;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class LabelAdapter extends RecyclerView.Adapter<LabelAdapter.LabelAdapterVH> {
    private Context context;
    private ArrayList<LabelModel> data;

    public LabelAdapter(Context context, ArrayList<LabelModel> data) {
        this.context = context;
        this.data = data;
    }

    @NonNull
    @Override
    public LabelAdapterVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.container_item_show_child, parent, false);
        return new LabelAdapterVH(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LabelAdapterVH holder, int position) {
        LabelModel model = data.get(position);
        holder.tvLabel.setText(model.getLabel());
        holder.civChild.setImageResource(model.getImageDrawable());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class LabelAdapterVH extends RecyclerView.ViewHolder {
        TextView tvLabel;
        CircleImageView civChild;
        public LabelAdapterVH(@NonNull View itemView) {
            super(itemView);

            tvLabel = itemView.findViewById(R.id.tvLabel);
            civChild = itemView.findViewById(R.id.civChild);
        }
    }
}
