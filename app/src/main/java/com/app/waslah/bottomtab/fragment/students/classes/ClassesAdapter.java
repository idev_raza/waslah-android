package com.app.waslah.bottomtab.fragment.students.classes;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.app.waslah.R;
import com.app.waslah.appconstant.AppConstant;
import com.app.waslah.dummymodel.DemoModel;
import com.app.waslah.dummymodel.LabelModel;
import com.app.waslah.dummymodel.displaymodel.HomeDisplayModel;
import com.app.waslah.bottomtab.fragment.students.labeladapter.LabelAdapter;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ClassesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private ArrayList<HomeDisplayModel> data;
    ArrayList<Integer> dataImage = new ArrayList<>();
    int page = 0;

    public ClassesAdapter(Context context, ArrayList<HomeDisplayModel> data) {
        this.context = context;
        this.data = data;
        /*
         * Add Dummy data
         * */
        dataImage.add(R.drawable.ic_user_d_forum);
        dataImage.add(R.drawable.ic_user_d_forum);
        dataImage.add(R.drawable.ic_user_d_forum);
        dataImage.add(R.drawable.ic_user_d_forum);
        dataImage.add(R.drawable.ic_user_d_forum);
        dataImage.add(R.drawable.ic_user_d_forum);
        dataImage.add(R.drawable.ic_user_d_forum);
        dataImage.add(R.drawable.ic_user_d_forum);
        dataImage.add(R.drawable.ic_user_d_forum);
        dataImage.add(R.drawable.ic_user_d_forum);
        dataImage.add(R.drawable.ic_user_d_forum);
        dataImage.add(R.drawable.ic_user_d_forum);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder vh = null;
        View view;
        switch (viewType) {
            case AppConstant.TYPE_TOP_BANNER:
                view = LayoutInflater.from(context).inflate(R.layout.adapter_banner_top, parent, false);
                vh = new ClassesAdapter.ClassesAdapterVH(view, viewType);
                break;

            case AppConstant.TYPE_CLASS:
                view = LayoutInflater.from(context).inflate(R.layout.adapter_item_classes, parent, false);
                vh = new ClassesAdapter.ClassesAdapterVH(view, viewType);
                break;
        }
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        final HomeDisplayModel model = data.get(position);
        switch (model.getType()) {
            case AppConstant.TYPE_TOP_BANNER:
                ArrayList<DemoModel.Data.Banner> dataBanner = new ArrayList<>();
                if (model.getDataBanner() != null && model.getDataBanner().size() > 0) {
                    dataBanner.addAll(model.getDataBanner());
                    SlidingBannerHomeAdapter slidingBannerHomeAdapter = new SlidingBannerHomeAdapter((Activity) context, dataBanner);
                    ((ClassesAdapter.ClassesAdapterVH) holder).viewPager.setAdapter(slidingBannerHomeAdapter);
                    addBottomDots(0, ((ClassesAdapter.ClassesAdapterVH) holder).llDots, model.getDataBanner());
                    ((ClassesAdapter.ClassesAdapterVH) holder).viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        @Override
                        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                        }

                        @Override
                        public void onPageSelected(int position) {
                            addBottomDots(position, ((ClassesAdapter.ClassesAdapterVH) holder).llDots, model.getDataBanner());
                            page = position;

                        }

                        @Override
                        public void onPageScrollStateChanged(int state) {

                        }
                    });

                }
                break;
            case AppConstant.TYPE_CLASS:
                ArrayList<DemoModel.Data.Clas> dataClass = new ArrayList<>();
                if (model.getDataClass() != null && model.getDataClass().size() > 0) {
                    dataClass.addAll(model.getDataClass());
                    for (int i = 0; i < dataClass.size(); i++) {
                        ((ClassesAdapter.ClassesAdapterVH) holder).tvClass.setText(dataClass.get(i).getName());
                        ((ClassesAdapter.ClassesAdapterVH) holder).tvLabel.setText(dataClass.get(i).getDuration());
                    }
                    /*
                     * Add Dummy data
                     * */
                    ArrayList<LabelModel> dataLabel = new ArrayList<>();
                    dataLabel.add(new LabelModel("Label", R.drawable.ic_user_d_forum));
                    dataLabel.add(new LabelModel("Label1", R.drawable.ic_user_d_forum));
                    dataLabel.add(new LabelModel("Label2", R.drawable.ic_user_d_forum));
                    dataLabel.add(new LabelModel("Label3", R.drawable.ic_user_d_forum));
                    dataLabel.add(new LabelModel("Label4", R.drawable.ic_user_d_forum));
                    dataLabel.add(new LabelModel("Label5", R.drawable.ic_user_d_forum));
                    dataLabel.add(new LabelModel("Label6", R.drawable.ic_user_d_forum));
                    dataLabel.add(new LabelModel("Label7", R.drawable.ic_user_d_forum));
                    dataLabel.add(new LabelModel("Label8", R.drawable.ic_user_d_forum));
                    dataLabel.add(new LabelModel("Label9", R.drawable.ic_user_d_forum));
                    /*
                     * Add Dummy data
                     * */
                    LinearLayoutManager linearLayout = new LinearLayoutManager(context);
                    linearLayout.setOrientation(LinearLayoutManager.HORIZONTAL);
                    ((ClassesAdapterVH) holder).rvHorizontal.setLayoutManager(linearLayout);
                    LabelAdapter labelAdapter = new LabelAdapter(context, dataLabel);
                    ((ClassesAdapterVH) holder).rvHorizontal.setAdapter(labelAdapter);
                }
                break;
        }
    }

    //
    private void addBottomDots(int currentPage, LinearLayout ll_dots, ArrayList<DemoModel.Data.Banner> dataBanner) {
        //
        TextView[] dots = new TextView[dataBanner.size()];
        ll_dots.removeAllViews();
        for (int i = 0; i < dots.length; i++) {

            dots[i] = new TextView(context);
            dots[i].setText(Html.fromHtml("&#8226;"));
            dots[i].setTextSize(20);
            dots[i].setTextColor(Color.parseColor("#000000"));
            ll_dots.addView(dots[i]);
        }


        if (dots.length > 0)
            dots[currentPage].setTextColor(Color.parseColor("#5EAE99"));
    }

    //
    private void setLeftMargin(CircleImageView civChild, int left, int top, int right, int bottom) {
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                140,
                140
        );
        //params.setLayoutDirection(LinearLayout.VERTICAL);
        civChild.getLayoutParams().height = 140;
        civChild.getLayoutParams().width = 140;
        civChild.setPadding(1, 1, 1, 1);
        civChild.setBackgroundResource(R.drawable.bg_round_white);
        params.setMargins(left, top, right, bottom);
        //tvLabel1.setLayoutParams(params);
        civChild.setLayoutParams(params);

    }

    private void setViewPagertabmargin(TabLayout tabLayout) {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            View tab = ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 0, 40, 0);

            tab.requestLayout();
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getType();
    }

    public class ClassesAdapterVH extends RecyclerView.ViewHolder {
        View view;
        ViewPager viewPager;
        LinearLayout llDots;
        TextView tvClass, tvLabel;
        RecyclerView rvHorizontal;

        public ClassesAdapterVH(@NonNull View itemView, int type) {
            super(itemView);
            view = itemView;
            switch (type) {
                case AppConstant.TYPE_TOP_BANNER:
                    viewPager = itemView.findViewById(R.id.viewPager);
                    llDots = itemView.findViewById(R.id.llDots);
                    break;
                case AppConstant.TYPE_CLASS:
                    tvClass = itemView.findViewById(R.id.tvClass);
                    tvLabel = itemView.findViewById(R.id.tvLabel);
                    rvHorizontal = itemView.findViewById(R.id.rvHorizontal);
                    break;

            }
        }
    }
}