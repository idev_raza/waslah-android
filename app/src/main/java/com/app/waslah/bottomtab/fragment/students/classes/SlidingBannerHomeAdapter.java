package com.app.waslah.bottomtab.fragment.students.classes;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.app.waslah.R;
import com.app.waslah.dummymodel.DemoModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class SlidingBannerHomeAdapter extends PagerAdapter {
    Activity activity;
    ArrayList<DemoModel.Data.Banner> data;



    public SlidingBannerHomeAdapter(Activity activity, ArrayList<DemoModel.Data.Banner> data) {
        this.activity = activity;
        this.data = data;

    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = layoutInflater.inflate(R.layout.adapter_item_slider_banner, container, false);
        ImageView ivSlider = view.findViewById(R.id.ivSlider);

        Glide.with(activity).load(R.drawable.ic_first)
                .apply(new RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.ic_user_d_forum)
                .error(R.drawable.ic_user_d_forum))
                .into(ivSlider);


        container.addView(view);

        return view;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        View view = (View) object;
        container.removeView(view);
    }




}
