package com.app.waslah.bottomtab.fragment.students;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.waslah.R;
import com.app.waslah.appconstant.AppConstant;
import com.app.waslah.bottomtab.fragment.students.classes.ClassesAdapter;
import com.app.waslah.appcontroller.deserializer.BooleanDeserializer;
import com.app.waslah.appcontroller.deserializer.DoubleDeserializer;
import com.app.waslah.appcontroller.deserializer.IntDeserializer;
import com.app.waslah.dummymodel.DemoModel;
import com.app.waslah.dummymodel.displaymodel.HomeDisplayModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class StudentsFragment extends Fragment {

    private String TAG = "dStudentsFragment";

    private RecyclerView rv;
    private ClassesAdapter adapter;
    private ArrayList<HomeDisplayModel> data = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_students, container, false);
        initView(view);
        getData();
        return view;
    }

    private void initView(View view) {
        rv = view.findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new ClassesAdapter(getActivity(), data);
        rv.setAdapter(adapter);

    }

    private void getData() {
        /*
         * Add Dummy data
         * */

        data.clear();
        DemoModel homeDataModel = getGsonAsConvert().fromJson(AppConstant.data, DemoModel.class);
        Log.d(TAG, " dataResult " + new Gson().toJson(homeDataModel));
        ArrayList<DemoModel.Data.Banner> dataBanner = new ArrayList<>();
        ArrayList<DemoModel.Data.Clas> dataClas = new ArrayList<>();
        HomeDisplayModel homeDisplayModel = null;// = new HomeDisplayModel();
        if (homeDataModel.getData().getBanners().size() > 0) {
            homeDisplayModel = new HomeDisplayModel();
            homeDisplayModel.setType(AppConstant.TYPE_TOP_BANNER);
            dataBanner.addAll(homeDataModel.getData().getBanners());
            homeDisplayModel.setDataBanner(dataBanner);
            data.add(homeDisplayModel);
        }
        for (DemoModel.Data.Clas clas : homeDataModel.getData().get_class()) {

            homeDisplayModel = new HomeDisplayModel();
            homeDisplayModel.setType(AppConstant.TYPE_CLASS);
            dataClas.add(clas);
            homeDisplayModel.setDataClass(dataClas);
            data.add(homeDisplayModel);
        }
        adapter.notifyDataSetChanged();
        /*
         * Add Dummy data
         * */

    }


    public static Gson getGsonAsConvert() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .registerTypeAdapter(Integer.class, new IntDeserializer())
                .registerTypeAdapter(Boolean.class, new BooleanDeserializer())
                .registerTypeAdapter(Double.class, new DoubleDeserializer())
                .create();
        return gson;
    }

}
