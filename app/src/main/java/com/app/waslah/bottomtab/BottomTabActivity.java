package com.app.waslah.bottomtab;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.waslah.R;
import com.app.waslah.bottomtab.fragment.activity.ActivityFragment;
import com.app.waslah.bottomtab.fragment.chat.ChatFragment;
import com.app.waslah.bottomtab.fragment.schedule.ScheduleFragment;
import com.app.waslah.bottomtab.fragment.students.StudentsFragment;
import com.app.waslah.changepassword.ChangePasswordActivity;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class BottomTabActivity extends AppCompatActivity implements View.OnClickListener {
    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private int[] tabIcons = {
            R.drawable.ic_tab_activity,
            R.drawable.ic_tab_chat,
            R.drawable.ic_tab_students,
            R.drawable.ic_tab_calendar
    };
    private TextView txtHeader;
    private ImageView ivBack, ivSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);
        //
        initHeader();
        //
        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabLayout);
        adapter = new TabAdapter(getSupportFragmentManager(), this);
        adapter.addFragment(new ActivityFragment(), getResources().getString(R.string.tab_activity), tabIcons[0]);
        adapter.addFragment(new ChatFragment(), getResources().getString(R.string.tab_chat), tabIcons[1]);
        adapter.addFragment(new StudentsFragment(), getResources().getString(R.string.tab_students), tabIcons[2]);
        adapter.addFragment(new ScheduleFragment(), getResources().getString(R.string.tab_schedule), tabIcons[3]);

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(2);
        highLightCurrentTab(2);// for initial selected tab view
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                highLightCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    //
    private void initHeader() {
        txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setVisibility(View.GONE);
        txtHeader.setText(getResources().getString(R.string.btn_signup));
        ivBack = findViewById(R.id.ivBack);
        ivSettings = findViewById(R.id.ivSettings);
        ivBack.setOnClickListener(this);
        ivSettings.setOnClickListener(this);
    }

    private void highLightCurrentTab(int position) {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(adapter.getTabView(i));
        }
        TabLayout.Tab tab = tabLayout.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(adapter.getSelectedTabView(position));
        if (position == 2) {
            txtHeader.setVisibility(View.VISIBLE);
            txtHeader.setText("Classes");
            txtHeader.setAllCaps(false);
            ivBack.setVisibility(View.GONE);
            ivSettings.setVisibility(View.VISIBLE);
        } else {
            txtHeader.setVisibility(View.GONE);
            txtHeader.setText("");
            ivBack.setVisibility(View.GONE);
            ivSettings.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivSettings:
                sendIntentToChangePassword();

                break;
        }
    }

    private void sendIntentToChangePassword() {
        startActivity(new Intent(this, ChangePasswordActivity.class));
    }

    public class TabAdapter extends FragmentStatePagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private final List<Integer> mFragmentIconList = new ArrayList<>();
        private Context context;

        TabAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        public void addFragment(Fragment fragment, String title, int tabIcon) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
            mFragmentIconList.add(tabIcon);
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {

            return null;
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public View getTabView(int position) {
            View view = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
            TextView tabTextView = view.findViewById(R.id.tabTextView);
            tabTextView.setText(mFragmentTitleList.get(position));
            tabTextView.setTextColor(ContextCompat.getColor(context, R.color.color_a9a9a9));
            ImageView tabImageView = view.findViewById(R.id.tabImageView);
            tabImageView.setImageResource(mFragmentIconList.get(position));
            tabImageView.setColorFilter(ContextCompat.getColor(context, R.color.color_a9a9a9), PorterDuff.Mode.SRC_ATOP);
            return view;
        }

        public View getSelectedTabView(int position) {
            View view = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);
            TextView tabTextView = view.findViewById(R.id.tabTextView);
            tabTextView.setText(mFragmentTitleList.get(position));
            tabTextView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimary));
            ImageView tabImageView = view.findViewById(R.id.tabImageView);
            tabImageView.setImageResource(mFragmentIconList.get(position));
            tabImageView.setColorFilter(ContextCompat.getColor(context, R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
            return view;
        }
    }
}

