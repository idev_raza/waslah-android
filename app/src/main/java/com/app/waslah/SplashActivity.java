package com.app.waslah;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.app.waslah.login.LoginActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sendIntentToLogin();
                finish();
            }
        }, 500);
    }

    private void sendIntentToLogin() {
        startActivity(new Intent(SplashActivity.this, LoginActivity.class));
    }
}
