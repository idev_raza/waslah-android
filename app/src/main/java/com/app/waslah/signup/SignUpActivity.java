package com.app.waslah.signup;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.app.waslah.R;
import com.app.waslah.userinfoscreen.UserInfoActivity;
import com.app.waslah.utils.JUtils;
import com.google.android.material.snackbar.Snackbar;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtHeader;
    private ImageView ivBack;
    private Button btnSignUp;
    private LinearLayout llMain;
    private EditText etFirstName, etLastName, etEmailAddress, etPassword, etPhoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initHeader();
        initView();
        btnSignUp = findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(this);
    }

    private void initView() {
        llMain = findViewById(R.id.llMain);
        //
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etEmailAddress = findViewById(R.id.etEmailAddress);
        etPassword = findViewById(R.id.etPassword);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
    }

    private void showSnackbar(String msg) {
        Snackbar snackbar;
        snackbar = Snackbar.make(llMain, msg, Snackbar.LENGTH_SHORT);
        View snackBarView = snackbar.getView();
        snackBarView.setBackground(getResources().getDrawable(R.drawable.text_shaded));
        TextView textView = snackBarView.findViewById(R.id.snackbar_text);
        textView.setTextColor(Color.RED);
        snackbar.show();
    }

    private void validateSignUp() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String email = etEmailAddress.getText().toString();
        String password = etPassword.getText().toString();
        String mobile = etPhoneNumber.getText().toString();
        if (TextUtils.isEmpty(firstName)) {
            showSnackbar("Please enter first name");
            return;
        }
        if (TextUtils.isEmpty(lastName)) {
            showSnackbar("Please enter last name");
            return;
        }
        if (TextUtils.isEmpty(email)) {
            showSnackbar("Please enter email");
            return;
        }
        if (!JUtils.isEmailValid(email)) {
            showSnackbar("Please enter valid email");
            return;
        }
        if (TextUtils.isEmpty(password)) {
            showSnackbar("Please enter password");
            return;
        }
        if (password.length() < 7) {
            showSnackbar("Password must be 7 digit long");
            return;
        }
        if (TextUtils.isEmpty(mobile)) {
            showSnackbar("Please enter mobile number");
            return;
        }
        if (mobile.length() != 10) {
            showSnackbar("Mobile number must be 10 digit long");
            return;
        }
        sendIntentToUserInfo();
    }

    private void sendIntentToUserInfo() {
        startActivity(new Intent(SignUpActivity.this, UserInfoActivity.class));
        finish();
    }

    private void initHeader() {
        txtHeader = findViewById(R.id.txtHeader);
        txtHeader.setText(getResources().getString(R.string.btn_signup));
        ivBack = findViewById(R.id.ivBack);
        ivBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.btnSignUp:
                validateSignUp();
                break;
        }
    }
}
